namespace King_Xiong.Migrations
{
    using King_Xiong.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<King_Xiong.Models.ApplicationDbContext>
    {


        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }


        protected override void Seed(King_Xiong.Models.ApplicationDbContext context)
        {
            //Admin role
            var roleManager = new RoleManager<IdentityRole>(
                new RoleStore<IdentityRole>(context));

            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
            }
            var userManager = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            if (!context.Users.Any(u => u.Email == "kxiong388@gmail.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    UserName = "kxiong388@gmail.com",
                    Email = "kxiong388@gmail.com",
                    FirstName = "King",
                    LastName = "Xiong",
                    DisplayName = "Kingaroo"
                }, "Asdf123!");

            }
            var adminId = userManager.FindByEmail("kxiong388@gmail.com").Id;
            userManager.AddToRole(adminId, "Admin");

            //Sean
            var roleModerator = new RoleManager<IdentityRole>(
               new RoleStore<IdentityRole>(context));

            if (!context.Roles.Any(r => r.Name == "Moderator"))
            {
                roleModerator.Create(new IdentityRole { Name = "Moderator" });
            }
            var userModerator = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            if (!context.Users.Any(u => u.Email == "sszpunar@coderfoundry.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    UserName = "sszpunar@coderfoundry.com",
                    Email = "sszpunar@coderfoundry.com",
                    FirstName = "Sean",
                    LastName = "Szpunar",
                    DisplayName = "Assistant"
                }, "Asdf123!");

            }
            var moderatorId = userModerator.FindByEmail("sszpunar@coderfoundry.com").Id;
            userModerator.AddToRole(moderatorId, "Moderator");

            //Eric
            if (!context.Roles.Any(r => r.Name == "Moderator"))
            {
                roleModerator.Create(new IdentityRole { Name = "Moderator" });
            }
            var secModerator = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            if (!context.Users.Any(u => u.Email == "ewatkins@coderfoundry.com"))
            {
                userManager.Create(new ApplicationUser
                {
                    UserName = "ewatkins@coderfoundry.com",
                    Email = "ewatkins@coderfoundry.com",
                    FirstName = "Eric",
                    LastName = "Watkins",
                    DisplayName = "Teacher"
                }, "Asdf123!");

            }
            var secModeratorId = userModerator.FindByEmail("ewatkins@coderfoundry.com").Id;
            userModerator.AddToRole(secModeratorId, "Moderator");
        }
    }
}