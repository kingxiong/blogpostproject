﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(King_Xiong.Startup))]
namespace King_Xiong
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
