﻿using King_Xiong.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace King_Xiong.Controllers

{   [RequireHttps]
    public class HomeController : UserController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "About Me";

            return View();
        }
        //must leave contact on to view the page Contact
        public ActionResult Contact()
        {
            ViewBag.Message = "Contact";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    var body = "<p>Email From: <b>{0}</b>({1})</p><p>Message:</p><p>{2}</p>";
                    //model.Body = "This is a message from your personal site. The name and the email of the contacting person is above";
                    var email = new MailMessage(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["emailTo"])
                    {
                        Subject = "Welcome to kingCom",
                        Body = string.Format(body, model.FromName, model.FromEmail, model.Body),
                        IsBodyHtml = true
                    };

                    var svc = new PersonalEmail();
                    await svc.SendAsync(email);

                    return RedirectToAction("Sent");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    await Task.FromResult(0);
                }

            }

            return View(model);
        }

        public ActionResult Portfolio()
        {
            ViewBag.Message = "Portfolio";

            return View();
        }
        public ActionResult Blog()
        {
            ViewBag.Message = "Blog";

            return View();
        }

        public ActionResult Sent()
        {
            return View();
        }
    }
}