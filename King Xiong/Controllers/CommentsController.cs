﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using King_Xiong.Models;
using Microsoft.AspNet.Identity;

namespace King_Xiong.Controllers
{
    [RequireHttps]
    public class CommentsController : UserController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Comments

        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.Author).Include(c => c.Post);
            return View(comments.ToList());
        }

        // GET: Comments/Details/5

        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);

        }
        // GET: Comments/Create

        public ActionResult Create()
        {
            ViewBag.AuthorId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PostId,AuthorId,Created,Body")] Comment comment)
        {

            if (ModelState.IsValid)
            {
                comment.AuthorId = User.Identity.GetUserId(); //adds using Microsoft.AspNet.Identity; 
                comment.Created = new DateTimeOffset(DateTime.Now);
                db.Comments.Add(comment);
                db.SaveChanges();
                var thisPost = db.Posts.Find(comment.PostId);
                if (thisPost != null)
                {
                    return RedirectToAction("Details", "BlogPosts", new { slug = thisPost.Slug });// redirect to details page in Blog Post. 
                }

            }

            ViewBag.AuthorId = new SelectList(db.Users, "Id", "FirstName", comment.AuthorId);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);

            return View(comment);

        }
        // GET: Comments/Edit/5


        [Authorize(Roles = "Admin, Moderator")]
        public ActionResult Edit(string slug, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            //ViewBag.AuthorId = new SelectList(db.Users, "Id", "FirstName", comment.AuthorId);
            ViewBag.Slug = slug;
            return View(comment);
        }


        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Moderator")]
        public ActionResult Edit([Bind(Include = "Id,PostId,AuthorId,Body,Created,Updated,UpdateReason")] Comment comment, string PostSlug)
        {

            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(PostSlug))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var post = db.Posts.FirstOrDefault(p => p.Slug == PostSlug);
                if (post == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    // look up post so that I can get the ID to set the PostID of the comment
                    comment.PostId = post.Id;

                    var userId = User.Identity.GetUserId();
                    comment.AuthorId = userId;
                    comment.Updated = DateTimeOffset.Now;

                    db.Entry(comment).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details", "Blog", new { slug = post.Slug });
                }
            }
            //ViewBag.AuthorId = new SelectList(db.Users, "Id", "FirstName", comment.AuthorId);
            //ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            return View(comment);
        }

        // GET: Comments/Delete/5
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Moderator")]
        public ActionResult DeleteConfirmed(int id)
        {
            var sameUser = User.Identity.GetUserId();
            Comment comment = db.Comments.Find(id);
            var slug = comment.Post.Slug;
            // comment.slug is deleted here but to save it's string value, we called it above,
            // that's why when we used it in the slug, it didnt work because
            // return RedirectToAction("Details", "BlogPosts", new { slug = comment.post.slug});
            // comment.post.slug does not exist after we remove it using the db.Comments.Remove(comment)
            // this 'comment' does not exist in the db, therefore we cannot find redirect to comment.post.slug afterwards
 
            db.Comments.Remove(comment);
            db.SaveChanges();



            //var commentPost = db.Posts.Find(comment.PostId);
            //if (commentPost != null)
            //{
            //return RedirectToAction("Index");

            // domain.com/blogposts/details/[slug = blogpost.title]
            return RedirectToAction("Details", "BlogPosts", new { slug = slug });

            //}
            //return View(comment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
