﻿//Standardized document function.

/*
(document).ready(function(){

  jQuery methods go here...

});*/
//You might have noticed that all jQuery methods in examples, are inside a document ready event
$(document).ready(function () {
    $("#calculate").click(function (event) {
        event.preventDefault();
    });
});

$("#calculate").click(calculateNumbers);

function calculateNumbers() {

    //input
    var firstValue = parseInt($("#firstNumber").val());
    //parseInt is converting input as an integer value instead of a text value
    var secondValue = parseInt($("#secondNumber").val());
    // $("#secondNumber") is using jquery to test if id="secondNumber" is a integer value.
    var thirdValue = parseInt($("#thirdNumber").val());
    var fourthValue = parseInt($("#fourthNumber").val());
    var fifthValue = parseInt($("#fifthNumber").val());


    //Processing 
    //inputing the variable min to be Math.min( , , , , ) 
    //min
    var min = Math.min(firstValue, secondValue, thirdValue, fourthValue, fifthValue);
    //max
    var max = Math.max(firstValue, secondValue, thirdValue, fourthValue, fifthValue);
    //sum
    var add = firstValue + secondValue + thirdValue + fourthValue + fifthValue;
    //product
    var numArray = [firstValue, secondValue, thirdValue, fourthValue, fifthValue];
    var multiplication = 1;
    for (i = 0; i < numArray.length; i++)
    {
        multiplication *= numArray[i];
    }

    /* Simplier version
    var multiplication = firstValue * secondValue * thirdValue * fourthValue * fifthValue;
    */

    //mean
    var mean = add / numArray.length;

    //output
    $("#least").html(min);
    $("#greatest").html(max);
    $("#mean").html(mean);
    $("#sum").html(add);
    // id="sum" exist in HTML, but .html(add) exist in var add in JavaScript
    $("#product").html(multiplication);

};


