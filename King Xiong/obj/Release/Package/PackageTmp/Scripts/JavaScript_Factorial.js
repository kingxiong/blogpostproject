﻿$(document).ready(function () {
    $("#factorialCalculate").click(function (event) {
        event.preventDefault();
    });
});


$("#factorialCalculate").click(calculateFactorial)

function calculateFactorial() {

    //input variables
    var factorialFunction = 1;
    var inputNum = parseInt($("#factorialNumber").val());

    //process
    for (i = inputNum; i > 0; i--) {
        factorialFunction *= i;
    }

    //output
    $("#factorial").html(factorialFunction);
}