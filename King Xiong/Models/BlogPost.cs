﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace King_Xiong.Models
{
    public class BlogPost
    {
        //constructor has the same name as its class
        //initializes an object of that type
        public BlogPost()
        {
            //'this' refers to the comment coming from Icollection<Comment> 

           this.Comments = new HashSet<Comment>();
        }
        public int Id { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset Created { get; set; }
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy}")]
        public DateTimeOffset? Updated { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public string MediaURL { get; set; }
        public bool Published { get; set; }

        //navigation property
        //In this scenerio, collection is a list of 'generic'Comment  object 
        public virtual ICollection<Comment> Comments { get; set; }
    }
}