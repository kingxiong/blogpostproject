﻿$(document).ready(function () {
    $("#confirmPal").click(function (event) {
        event.preventDefault();
    });
});

$("#confirmPal").click(computing);

function computing() {

    //declares variable input
    //.val() is a method in jQuery used to put value in an object.
    var inputWord = $("#inputWord").val();
    var theText = [];

    //computing the results
    for (j = 0; j <= inputWord.length - 1; j++) {
        //setting the array theText to be the same as each individual character from the inputed word.
        theText[j] = inputWord.charAt(j);
    }

    theText.reverse();
    //theText.reverse() converts the inputed word around
    var oppText = theText.join("");
    //oppText changes the theText.join("") into the array with no commas in between
    //example (["a","f","t","e","r"] ==> "after")

    //output
    if (inputWord === oppText) {
        $("#outcome").html("It is a palindrome");
    } else if (theText !== oppText) {
        $("#outcome").html("It is not a palindrome");
    }

}
